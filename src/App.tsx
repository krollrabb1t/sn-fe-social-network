import React, { Dispatch, useEffect } from 'react';
import './App.css';
import { Route, Routes } from 'react-router';
import { SignIn } from './pages/Account/SignIn';
import { SignUp } from './pages/Account/SignUp';
import { Feed } from './pages/MainContent/Feed';
import { hasAccessToken } from './middlewares/auth';
import { useNavigate } from 'react-router-dom';
import { getAccount } from './api/account';
import { setAccount } from './store/actions';
import { useDispatch } from 'react-redux';

export const App = () => {
  const navigate = useNavigate();
  const dispatch: Dispatch<any> = useDispatch();

  useEffect(() => {
    if (hasAccessToken()) {
      getAccount().then((res) => {
        dispatch(setAccount(res));
      });
      navigate('/feed');
    } else {
      if (window.location.pathname !== '/sign-in' && window.location.pathname !== '/sign-up') {
        navigate('/sign-in');
      }
    }
  }, []);

  return (
    <>
      <Routes>
        <Route element={<SignIn />} path="/sign-in" />
        <Route element={<SignUp />} path="/sign-up" />
        <Route element={<Feed />} path="/feed" />
      </Routes>
    </>
  );
};

export default App;
