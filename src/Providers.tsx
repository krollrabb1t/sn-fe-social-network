import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { Store } from 'redux';
import reducer from './store/reducer';
import { AppAction, AppState, DispatchType } from './store/types';
import { configureStore } from '@reduxjs/toolkit';

export const Providers = ({ children }: { children: React.ReactNode }) => {
  const store: Store<AppState, AppAction> & {
    dispatch: DispatchType;
  } = configureStore({ reducer });
  return (
    <Provider store={store}>
      <BrowserRouter>{children}</BrowserRouter>
    </Provider>
  );
};
