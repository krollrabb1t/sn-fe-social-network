import { AccountSignInType, AccountSignUpType } from '../types';
import { DEV_API } from '../settings';
import axios from 'axios';
import { serverGet } from './base';

export const createAccount = (accountData: AccountSignUpType) => {
  return axios.post(`${DEV_API}/account/sign-up`, accountData);
};

export const signInAccount = (accountData: AccountSignInType) => {
  const params = new URLSearchParams();
  params.append('username', accountData.email);
  params.append('password', accountData.password);
  return axios.post(`${DEV_API}/account/sign-in`, params, {
    withCredentials: true
  });
};

export const getAccount = () => {
  return serverGet('/account', {});
};

export const checkAuth = () => {
  return serverGet('/account/check-auth', {});
};
