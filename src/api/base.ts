import axios from 'axios';
import { DEV_API } from '../settings';

export const serverGet = async (URI: string, params: any) => {
  const access_token = localStorage.getItem('access_token');

  const res = await axios.get(`${DEV_API}${URI}`, {
    headers: { Authorization: `Bearer ${access_token}` },
    params: params
  });

  return res.data;
};

export const serverPostParams = async (URI: string, params: any) => {
  const access_token = localStorage.getItem('access_token');
  const res = await axios.post(`${DEV_API}${URI}`, {
    headers: { Authorization: `Bearer ${access_token}` },
    params: params
  });

  return res.data;
};

export const serverPostJSON = async (URI: string, params: any) => {
  const access_token = localStorage.getItem('access_token');

  const res = await axios.post(`${DEV_API}${URI}`, params, {
    headers: {
      Authorization: `Bearer ${access_token}`,
      'Content-Type': 'Application/json'
    }
  });

  return res.data;
};

export const serverDelete = async (URI: string) => {
  const access_token = localStorage.getItem('access_token');

  const res = await axios.delete(`${DEV_API}${URI}`, {
    headers: {
      Authorization: `Bearer ${access_token}`,
      'Content-Type': 'Application/json'
    }
  });

  return res.data;
};
