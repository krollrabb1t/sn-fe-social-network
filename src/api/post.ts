import { serverGet, serverPostJSON } from './base';
import { PostCreateType, PostDeleteType } from '../types';
import { PostLikeType, PostUnlikeType } from '../components/feed/types';

export const listPosts = () => {
  return serverGet('/post/all', {});
};

export const createNewPost = (post: PostCreateType) => {
  return serverPostJSON('/post/create', post);
};

export const deletePost = (post: PostDeleteType) => {
  return serverPostJSON('/post/delete', post);
};

export const likePost = (post: PostLikeType) => {
  return serverPostJSON('/post/like', post);
};

export const unlikePost = (post: PostUnlikeType) => {
  return serverPostJSON('/post/unlike', post);
};
