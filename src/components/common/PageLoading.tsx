import Box from '@mui/material/Box';
import { CircularProgress } from '@mui/material';
import * as React from 'react';

export const PageLoading = () => {
  return (
    <Box
      sx={{
        position: 'fixed',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
      }}>
      <CircularProgress />
    </Box>
  );
};
