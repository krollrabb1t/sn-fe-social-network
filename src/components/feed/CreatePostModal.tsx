import * as React from 'react';
import Backdrop from '@mui/material/Backdrop';
import Box from '@mui/material/Box';
import Modal from '@mui/material/Modal';
import Fade from '@mui/material/Fade';
import { Dispatch, SetStateAction, useEffect, useState } from 'react';
import { TextField } from '@mui/material';
import Container from '@mui/material/Container';
import CardHeader from '@mui/material/CardHeader';
import CardContent from '@mui/material/CardContent';
import Card from '@mui/material/Card';
import Button from '@mui/material/Button';
import { useDispatch, useSelector } from 'react-redux';
import { setFeedPosts } from '../../store/actions';
import { createNewPost } from '../../api/post';
import { useForm, Controller } from 'react-hook-form';
import { AccountSignInType, PostCreateType } from '../../types';
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup';
import { AppState } from '../../store/types';

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  width: '50%',
  transform: 'translate(-50%, -50%)',
  bgcolor: 'background.paper',
  boxShadow: 24,
  p: 4,
  alignItems: 'center'
};

export const CreatePostModal = ({
  show,
  setShow
}: {
  show: boolean;
  setShow: Dispatch<SetStateAction<boolean>>;
}) => {
  const validationSchema = Yup.object().shape({
    title: Yup.string().required(),
    body: Yup.string().required()
  });

  const dispatch: Dispatch<any> = useDispatch();
  const posts = useSelector((state: AppState) => state.feed.posts);
  const account = useSelector((state: AppState) => state.account);
  const [isSaving, setIsSaving] = useState(false);
  const {
    control,
    handleSubmit,
    formState: { errors },
    setValue,
    reset
  } = useForm<PostCreateType>({
    resolver: yupResolver(validationSchema),
    defaultValues: {
      title: '',
      body: '',
      accountId: account.id
    }
  });

  const onSubmit = (data: PostCreateType) => {
    setValue('accountId', account.id);
    setIsSaving(true);
    createNewPost(data).then((res) => {
      if (res && !isSaving) {
        setIsSaving(false);
        reset();
        setShow(false);
        dispatch(setFeedPosts([...posts, res]));
      }
    });
  };

  return (
    <div>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        open={show}
        onClose={() => {
          setShow(false);
        }}
        closeAfterTransition
        slots={{ backdrop: Backdrop }}
        slotProps={{
          backdrop: {
            timeout: 500
          }
        }}>
        <Fade in={show}>
          <form>
            <Box sx={style}>
              <Card sx={{ mb: 4, maxWidth: 600 }}>
                <CardHeader
                  sx={{ mb: 2 }}
                  title="Post creation"
                  subheader={
                    <Controller
                      name="title"
                      control={control}
                      render={({ field: { onChange, value } }) => {
                        return (
                          <TextField
                            onChange={onChange}
                            value={value}
                            fullWidth={true}
                            id="standard-basic"
                            error={!!errors.title}
                            helperText={errors.title ? 'Title is required' : ''}
                            label="Title"
                            variant="standard"
                          />
                        );
                      }}
                    />
                  }
                />
                <CardContent>
                  <Controller
                    name="body"
                    control={control}
                    render={({ field: { onChange, value } }) => {
                      return (
                        <TextField
                          onChange={onChange}
                          value={value}
                          fullWidth={true}
                          error={!!errors.body}
                          helperText={errors.title ? 'Body is required' : ''}
                          sx={{ minHeight: '200px' }}
                          id="outlined-textarea"
                          label="Type something..."
                          multiline
                          variant="outlined"
                          inputProps={{ style: { resize: 'both', minHeight: '200px' } }}
                        />
                      );
                    }}
                  />
                </CardContent>
              </Card>
              <Container sx={{ display: 'flex', justifyContent: 'space-between' }}>
                <Button disabled={isSaving} variant="contained" onClick={handleSubmit(onSubmit)}>
                  Create
                </Button>
                <Button variant="outlined">Close</Button>
              </Container>
            </Box>
          </form>
        </Fade>
      </Modal>
    </div>
  );
};
