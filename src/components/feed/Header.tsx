import React, { Dispatch, SetStateAction } from 'react';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import { useNavigate } from 'react-router-dom';

export const Header = ({ setShow }: { setShow: Dispatch<SetStateAction<boolean>> }) => {
  const navigate = useNavigate();
  const logout = () => {
    localStorage.removeItem('access_token');
    navigate('/sign-in');
  };

  return (
    <Box sx={{ display: 'flex', justifyContent: 'space-between', maxWidth: 600, mt: 1 }}>
      <Button
        variant="contained"
        onClick={() => {
          setShow(true);
        }}>
        Create Post
      </Button>
      <Button onClick={() => logout()}>Log out</Button>
    </Box>
  );
};
