import * as React from 'react';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardContent from '@mui/material/CardContent';
import CardActions from '@mui/material/CardActions';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import FavoriteIcon from '@mui/icons-material/Favorite';
import { PostType } from './types';
import { Tooltip } from '@mui/material';
import CloseIcon from '@mui/icons-material/Close';
import { useDispatch, useSelector } from 'react-redux';
import { AppState } from '../../store/types';
import { Dispatch, useEffect } from 'react';
import { setFeedPosts, setPostLike, setPostUnlike } from '../../store/actions';
import { deletePost, likePost, unlikePost } from '../../api/post';

export const Post = ({ post }: { post: PostType }) => {
  const account = useSelector((state: AppState) => state.account);
  const posts = useSelector((state: AppState) => state.feed.posts);
  const dispatch: Dispatch<any> = useDispatch();

  const getLikedByList = () => {
    const likedByText = 'Liked by\n';
    if (post.likes.length) {
      const andOthersText =
        post.likes.length > 3 ? ' and ' + (post.likes.length - 3) + ' others' : '';
      return (
        likedByText +
        post.likes
          .slice(0, 3)
          .map((like) => like.likedByAccount.lastName + ' ' + like.likedByAccount.lastName)
          .join(', ') +
        andOthersText
      );
    }
  };

  const removePost = () => {
    deletePost({ postId: post.id, accountId: account.id }).then((res) => {
      if (res) {
        dispatch(setFeedPosts(posts.filter((p) => p.id !== post.id)));
      }
    });
  };

  const handleLike = () => {
    if (post.likes.filter((like) => like.likedByAccount.id === account.id).length) {
      const postLikeObject = {
        postId: post.id,
        accountId: account.id
      };
      unlikePost(postLikeObject).then((res) => {
        if (res) {
          dispatch(setPostUnlike(postLikeObject));
        }
      });
    } else {
      const postLikeObject = {
        postId: post.id,
        createdDatetime: new Date().toISOString(),
        likedByAccount: {
          id: account.id,
          firstName: account.firstName,
          lastName: account.lastName
        }
      };
      likePost(postLikeObject).then((res) => {
        if (res) {
          dispatch(setPostLike(postLikeObject));
        }
      });
    }
  };

  return (
    <Card sx={{ width: '100%', padding: 0 }}>
      <CardHeader
        title={post.title}
        subheader={new Date(post.createdDatetime).toDateString()}
        action={
          account.id === post.accountId && (
            <IconButton onClick={() => removePost()}>
              <CloseIcon />
            </IconButton>
          )
        }
      />
      <CardContent>
        <Typography variant="body2" color="text.secondary">
          {post.body}
        </Typography>
      </CardContent>
      <CardActions sx={{ justifyContent: 'flex-end' }} disableSpacing>
        <IconButton onClick={() => handleLike()} aria-label="add to favorites">
          <Tooltip style={{ minWidth: 200 }} title={post.likes.length ? getLikedByList() : 'Like'}>
            <FavoriteIcon
              style={{
                color: post.likes.filter((like) => like.likedByAccount.id === account.id).length
                  ? 'red'
                  : 'grey'
              }}
            />
          </Tooltip>
        </IconButton>
      </CardActions>
    </Card>
  );
};
