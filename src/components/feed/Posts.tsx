import Box from '@mui/material/Box';
import { Post } from './Post';
import React, { Dispatch, useEffect, useState } from 'react';
import { PostType } from './types';
import { useDispatch, useSelector } from 'react-redux';
import { listPosts } from '../../api/post';
import { setFeedPosts } from '../../store/actions';
import { PageLoading } from '../../components/common/PageLoading';
import { useNavigate } from 'react-router-dom';
import { AppState } from '../../store/types';

export const Posts = () => {
  const [initialLoading, setInitialLoading] = useState(true);
  const posts = useSelector((state: AppState) => state.feed.posts);
  const navigate = useNavigate();

  const dispatch: Dispatch<any> = useDispatch();

  useEffect(() => {
    if (!localStorage.getItem('access_token')) {
      navigate('/sign-in');
    }
    listPosts()
      .then((res) => {
        dispatch(setFeedPosts(res));
      })
      .catch(() => {
        navigate('/sign-in');
        localStorage.removeItem('access_token');
      });
    setTimeout(() => {
      setInitialLoading(false);
    }, 300);
  }, []);

  return initialLoading ? (
    <PageLoading />
  ) : (
    <Box sx={{ mt: 10, mb: 6 }}>
      <Box
        sx={{
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
          gap: '30px',
          maxWidth: 600
        }}>
        {posts.map((post) => (
          <Post post={post} key={post.id} />
        ))}
      </Box>
    </Box>
  );
};
