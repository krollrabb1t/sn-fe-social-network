export type PostLikedByAccountType = {
  id: number;
  firstName: string;
  lastName: string;
};

export type PostLikeType = {
  postId: number;
  createdDatetime: string;
  likedByAccount: PostLikedByAccountType;
};

export type PostUnlikeType = {
  postId: number;
  accountId: number;
};

export type PostType = {
  id: number;
  accountId: number;
  title: string;
  body: string;
  createdDatetime: string;
  likes: Array<PostLikeType>;
};

export type DeletePostType = {
  id: number;
};
