export const hasAccessToken = () => {
  if (localStorage.getItem('access_token')) {
    return true;
  }
};
