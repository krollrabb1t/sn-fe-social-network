import * as React from 'react';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Checkbox from '@mui/material/Checkbox';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import * as Yup from 'yup';
import { passwordPattern } from '../../utils';
import { yupResolver } from '@hookform/resolvers/yup';
import { Controller, useForm } from 'react-hook-form';
import { checkAuth, getAccount, signInAccount } from '../../api/account';
import { Dispatch, useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import LinearProgress from '@mui/material/LinearProgress';
import { PageLoading } from '../../components/common/PageLoading';
import { AccountSignInType } from '../../types';
import { useDispatch } from 'react-redux';
import { setAccount } from '../../store/actions';

export const SignIn = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [initialLoading, setInitialLoading] = useState(true);
  const navigate = useNavigate();
  const dispatch: Dispatch<any> = useDispatch();

  const validationSchema = Yup.object().shape({
    email: Yup.string().required().email()
    // password: Yup.string().matches(passwordPattern)
  });

  const {
    control,
    handleSubmit,
    formState: { errors }
  } = useForm<AccountSignInType>({
    resolver: yupResolver(validationSchema),
    defaultValues: {
      email: '',
      password: '',
      rememberMe: false
    }
  });

  const onSubmit = (data: AccountSignInType) => {
    setIsLoading(true);
    signInAccount(data).then((result) => {
      localStorage.setItem('access_token', result.data.accessToken);
      getAccount().then((res) => {
        dispatch(setAccount(res));
      });
      navigate('/feed');
    });
  };

  useEffect(() => {
    setTimeout(() => {
      setInitialLoading(false);
    }, 800);
  }, []);

  return initialLoading ? (
    <PageLoading />
  ) : (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <form>
        <Box
          sx={{
            marginTop: '80%',
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center'
          }}>
          <Typography component="h1" variant="h5">
            Sign in
          </Typography>
          <Box sx={{ mt: 1 }}>
            <Controller
              name="email"
              control={control}
              rules={{ required: true }}
              render={({ field: { onChange, value } }) => {
                return (
                  <TextField
                    onChange={onChange}
                    value={value}
                    error={!!errors.email}
                    helperText={errors.email ? 'Email is required' : ''}
                    margin="normal"
                    required
                    fullWidth
                    id="email"
                    label="Email Address"
                    name="email"
                    autoComplete="email"
                    autoFocus
                  />
                );
              }}
            />
            <Controller
              name="password"
              control={control}
              rules={{ required: true }}
              render={({ field: { onChange, value } }) => {
                return (
                  <TextField
                    error={!!errors.password}
                    helperText={errors?.password ? 'Check your password' : ''}
                    onChange={onChange}
                    value={value}
                    margin="normal"
                    required
                    fullWidth
                    name="password"
                    label="Password"
                    type="password"
                    id="password"
                    autoComplete="current-password"
                  />
                );
              }}
            />
            <Controller
              name="rememberMe"
              control={control}
              render={({ field: { onChange, value } }) => (
                <Checkbox onChange={onChange} checked={!!value} />
              )}
            />
            <label htmlFor="rememberMe">Remember me</label>
            <Button
              onClick={handleSubmit(onSubmit)}
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: isLoading ? 0 : 2 }}>
              Sign In
            </Button>
            {isLoading && (
              <Box sx={{ width: '100%', mb: 2 }}>
                <LinearProgress />
              </Box>
            )}
            <Grid container justifyContent="center">
              <Link href="/sign-up" variant="body2">
                {"Don't have an account? Sign Up"}
              </Link>
            </Grid>
          </Box>
        </Box>
      </form>
    </Container>
  );
};
