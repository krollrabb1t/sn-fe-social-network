import * as React from 'react';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { Controller, useForm } from 'react-hook-form';
import * as Yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';
import { checkAuth, createAccount } from '../../api/account';
import { AccountSignUpType } from '../../types';
import { passwordPattern } from '../../utils';
import LinearProgress from '@mui/material/LinearProgress';
import { useNavigate } from 'react-router-dom';
import { useEffect, useState } from 'react';
import { CircularProgress } from '@mui/material';
import { PageLoading } from '../../components/common/PageLoading';

type SignUpFormType = {
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  passwordConfirmation: string;
};

export const SignUp = () => {
  const navigate = useNavigate();
  const [isLoading, setIsLoading] = useState(false);
  const [initialLoading, setInitialLoading] = useState(true);

  useEffect(() => {
    if (localStorage.getItem('access_token')) {
      checkAuth().then((res) => {
        if (res?.expire) {
          navigate('/feed');
        }
      });
    }
    setTimeout(() => {
      setInitialLoading(false);
    }, 800);
  }, []);

  const validationSchema = Yup.object().shape({
    firstName: Yup.string().required(),
    lastName: Yup.string().required(),
    email: Yup.string().required().email()
    // password: Yup.string().required().min(6).matches(passwordPattern),
    // passwordConfirmation: Yup.string().oneOf([Yup.ref('password')])
  });

  const {
    control,
    handleSubmit,
    formState: { errors }
  } = useForm<SignUpFormType>({
    resolver: yupResolver(validationSchema),
    defaultValues: {
      firstName: '',
      lastName: '',
      email: '',
      password: '',
      passwordConfirmation: ''
    }
  });

  const onSubmit = (input: SignUpFormType) => {
    if (
      !(
        errors.password &&
        errors.passwordConfirmation &&
        errors.email &&
        errors.firstName &&
        errors.lastName
      )
    ) {
      const signUpData: AccountSignUpType = {
        firstName: input.firstName,
        lastName: input.lastName,
        email: input.email,
        password: input.password
      };
      setIsLoading(true);
      createAccount(signUpData)
        .then((res) => {
          navigate('/sign-in');
        })
        .catch((err) => {
          console.log(err);
        });
    }
  };

  return initialLoading ? (
    <PageLoading />
  ) : (
    <Container component="main" maxWidth="xs">
      <Box
        sx={{
          marginTop: '80%',
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center'
        }}>
        <Typography component="h1" variant="h5" sx={{ marginBottom: '10%' }}>
          Sign up
        </Typography>
        <form>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <Controller
                name="firstName"
                control={control}
                rules={{ required: true }}
                render={({ field: { onChange, value } }) => {
                  return (
                    <TextField
                      error={!!errors.firstName}
                      onChange={onChange}
                      value={value}
                      autoComplete="given-name"
                      name="firstName"
                      fullWidth
                      id="firstName"
                      label="First Name"
                      autoFocus
                    />
                  );
                }}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <Controller
                name="lastName"
                control={control}
                rules={{ required: true }}
                render={({ field: { onChange, value } }) => {
                  return (
                    <TextField
                      error={!!errors.lastName}
                      onChange={onChange}
                      value={value}
                      fullWidth
                      id="lastName"
                      label="Last Name"
                      name="lastName"
                      autoComplete="family-name"
                    />
                  );
                }}
              />
            </Grid>
            <Grid item xs={12}>
              <Controller
                name="email"
                control={control}
                render={({ field: { onChange, value } }) => {
                  return (
                    <TextField
                      error={Boolean(errors['email'])}
                      helperText={errors['email'] ? 'Invalid email' : ''}
                      onChange={onChange}
                      value={value}
                      fullWidth
                      id="email"
                      label="Email Address"
                      name="email"
                      autoComplete="email"
                    />
                  );
                }}
              />
            </Grid>
            <Grid item xs={12}>
              <Controller
                name="password"
                control={control}
                render={({ field: { onChange, value } }) => {
                  return (
                    <TextField
                      error={Boolean(errors['password'])}
                      helperText={
                        errors['password']
                          ? 'Your password must be at least 8 characters, uppercase, lowercase, and number'
                          : ''
                      }
                      onChange={onChange}
                      value={value}
                      fullWidth
                      name="password"
                      label="Password"
                      type="password"
                      id="password"
                      autoComplete="new-password"
                    />
                  );
                }}
              />
            </Grid>
            <Grid item xs={12}>
              <Controller
                name="passwordConfirmation"
                control={control}
                render={({ field: { onChange, value } }) => {
                  return (
                    <TextField
                      error={Boolean(errors['password'])}
                      helperText={errors['password'] ? 'Passwords must match' : ''}
                      onChange={onChange}
                      value={value}
                      fullWidth
                      name="passwordConfirmation"
                      label="Confirm Password"
                      type="password"
                      id="passwordConfirmation"
                      autoComplete="new-password"
                    />
                  );
                }}
              />
            </Grid>
          </Grid>
          <Button
            onClick={handleSubmit(onSubmit)}
            fullWidth
            variant="contained"
            sx={{ mt: 3, mb: isLoading ? 0 : 2 }}>
            Sign Up
          </Button>
          {isLoading && (
            <Box sx={{ width: '100%', mb: 2 }}>
              <LinearProgress />
            </Box>
          )}
        </form>
        <Grid container justifyContent="center">
          <Link href="/sign-in" variant="body2">
            Already have an account? Sign in
          </Link>
        </Grid>
      </Box>
    </Container>
  );
};
