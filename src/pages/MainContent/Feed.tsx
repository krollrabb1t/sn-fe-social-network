import React, { useState } from 'react';
import { Posts } from '../../components/feed/Posts';
import { Header } from '../../components/feed/Header';
import { useNavigate } from 'react-router-dom';
import { CreatePostModal } from '../../components/feed/CreatePostModal';
import Box from '@mui/material/Box';

export const Feed = () => {
  const [showCreatePostModal, setShowCreatePostModal] = useState(false);

  return (
    <Box
      sx={{
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'column',
        maxWidth: 600,
        margin: 'auto'
      }}>
      <Header setShow={setShowCreatePostModal} />
      <Posts />;
      <CreatePostModal show={showCreatePostModal} setShow={setShowCreatePostModal} />
    </Box>
  );
};
