import { AccountState } from './types';
import { SET_ACCOUNT, SET_FEED_POSTS, SET_POST_LIKE, SET_POST_UNLIKE } from './actionTypes';
import { PostLikeType, PostType, PostUnlikeType } from '../components/feed/types';

export const setAccount = (account: AccountState) => {
  return {
    type: SET_ACCOUNT,
    data: account
  };
};

export const setFeedPosts = (posts: Array<PostType>) => {
  return {
    type: SET_FEED_POSTS,
    data: posts
  };
};

export const setPostLike = (post: PostLikeType) => {
  return {
    type: SET_POST_LIKE,
    data: post
  };
};

export const setPostUnlike = (post: PostUnlikeType) => {
  return {
    type: SET_POST_UNLIKE,
    data: post
  };
};
