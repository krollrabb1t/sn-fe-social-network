import * as actionTypes from './actionTypes';
import { AppAction, AppState } from './types';
import { SET_POST_UNLIKE } from './actionTypes';

const initialState: AppState = {
  account: {
    id: 0,
    firstName: '',
    lastName: '',
    email: '',
    createdDatetime: ''
  },
  feed: {
    posts: []
  }
};

const reducer = (state: AppState = initialState, action: AppAction) => {
  switch (action.type) {
    case actionTypes.SET_ACCOUNT:
      return {
        ...state,
        account: action.data
      };
    case actionTypes.SET_FEED_POSTS:
      return {
        ...state,
        feed: {
          ...state.feed,
          posts: action.data
        }
      };
    case actionTypes.SET_POST_LIKE:
      return {
        ...state,
        feed: {
          ...state.feed,
          posts: state.feed.posts.map((post) => {
            if (post.id === action.data.postId) {
              return {
                ...post,
                likes: [
                  ...post.likes,
                  {
                    id: action.data.postId,
                    createdDatetime: action.data.createdDatetime,
                    likedByAccount: {
                      id: action.data.likedByAccount.id,
                      firstName: action.data.likedByAccount.firstName,
                      lastName: action.data.likedByAccount.lastName
                    }
                  }
                ]
              };
            }
            return post;
          })
        }
      };
    case actionTypes.SET_POST_UNLIKE:
      return {
        ...state,
        feed: {
          ...state.feed,
          posts: state.feed.posts.map((post) => {
            if (post.id === action.data.postId) {
              return {
                ...post,
                likes: post.likes.filter((like) => {
                  return like.likedByAccount.id !== action.data.accountId;
                })
              };
            }
            return post;
          })
        }
      };
  }
  return state;
};

export default reducer;
