import { DeletePostType, PostType } from '../components/feed/types';

export type AccountState = {
  id: number;
  firstName: string;
  lastName: string;
  email: string;
  createdDatetime: string;
};

export type FeedState = {
  posts: Array<PostType>;
};

export type AppState = {
  account: AccountState;
  feed: FeedState;
};

export type AppAction = {
  type: string;
  // data: AccountState | PostType | DeletePostType;
  data: any;
};

export type DispatchType = (args: AppAction) => AppAction;
