export type AccountSignUpType = {
  firstName: string;
  lastName: string;
  email: string;
  password: string;
};

export type AccountSignInType = {
  email: string;
  password: string;
  rememberMe: boolean;
};

export type PostCreateType = {
  title: string;
  body: string;
  accountId: number;
};

export type PostDeleteType = {
  postId: number;
  accountId: number;
};
