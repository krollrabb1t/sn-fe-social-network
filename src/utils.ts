export const emailPattern = /[a-z0-9]+@[a-z]+.[a-z]{2,3}/;

export const passwordPattern = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$/;

export const isValidPassword = (password: string) => {
  const regex = new RegExp(passwordPattern);
  return regex.test(password);
};

export const isValidEmail = (email: string) => {
  const regex = new RegExp(emailPattern);
  return regex.test(email);
};
